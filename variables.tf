variable "aws_ecs_cluster_id" {
  type = string
  description = "ECS cluster ID"
}

variable "vpc_id" {
 type = string
 description = "ID of VPC"
}

variable "load_balancer_arn" {
  type = string
  description = "ARN of Application Load balancer"
}

variable "default_target_group_arn" {
  type = string
  description = "ARN of the target group associated to Application Load balancer"  
}


variable "ecs_iam_role_arn" {
  type = string
  description = "ARN of the ECS IAM role"
}

variable "service" {
  type = object({
    name = string,
    container_name = string,
    container_port = number,
    host_port = number
    protocol = string,
    health_check_path = string 
    desired_count = number

    task_definition_arn = string
  })

  description = "Provide service as object"
}


variable "acm_certificate_arn" {
    type = string
    description = "ARN to SSL certificate in AWS ACM"    
}

variable "create_aws_alb_listener_rule" {
  description = "Controls if aws_alb_listener_rule resources should be created"
  type        = bool
  default     = false
}

variable "aws_alb_listener_rule_path" {
  description = "Path that the service will be accessed"
  type        = string
  default     = "/backend/*"
}