# Description

This module allows us to create/deploy a service inside a ECS cluster

Sample of variable input are in the "fixtures.tfvars" file. You may need to adjust it.

## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_alb_listener.http](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/alb_listener) | resource |
| [aws_alb_listener.https](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/alb_listener) | resource |
| [aws_alb_listener_rule.http](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/alb_listener_rule) | resource |
| [aws_alb_listener_rule.https](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/alb_listener_rule) | resource |
| [aws_alb_target_group.service-target-group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/alb_target_group) | resource |
| [aws_ecs_service.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecs_service) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_acm_certificate_arn"></a> [acm\_certificate\_arn](#input\_acm\_certificate\_arn) | ARN to SSL certificate in AWS ACM | `string` | n/a | yes |
| <a name="input_aws_alb_listener_rule_path"></a> [aws\_alb\_listener\_rule\_path](#input\_aws\_alb\_listener\_rule\_path) | Path that the service will be accessed | `string` | `"/backend/*"` | no |
| <a name="input_aws_ecs_cluster_id"></a> [aws\_ecs\_cluster\_id](#input\_aws\_ecs\_cluster\_id) | ECS cluster ID | `string` | n/a | yes |
| <a name="input_create_aws_alb_listener_rule"></a> [create\_aws\_alb\_listener\_rule](#input\_create\_aws\_alb\_listener\_rule) | Controls if aws\_alb\_listener\_rule resources should be created | `bool` | `false` | no |
| <a name="input_default_target_group_arn"></a> [default\_target\_group\_arn](#input\_default\_target\_group\_arn) | ARN of the target group associated to Application Load balancer | `string` | n/a | yes |
| <a name="input_ecs_iam_role_arn"></a> [ecs\_iam\_role\_arn](#input\_ecs\_iam\_role\_arn) | ARN of the ECS IAM role | `string` | n/a | yes |
| <a name="input_load_balancer_arn"></a> [load\_balancer\_arn](#input\_load\_balancer\_arn) | ARN of Application Load balancer | `string` | n/a | yes |
| <a name="input_service"></a> [service](#input\_service) | Provide service as object | <pre>object({<br>    name = string,<br>    container_name = string,<br>    container_port = number,<br>    host_port = number<br>    protocol = string,<br>    health_check_path = string <br>    desired_count = number<br><br>    task_definition_arn = string<br>  })</pre> | n/a | yes |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | ID of VPC | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_aws_alb_target_group_arn"></a> [aws\_alb\_target\_group\_arn](#output\_aws\_alb\_target\_group\_arn) | n/a |
| <a name="output_aws_alb_target_group_name"></a> [aws\_alb\_target\_group\_name](#output\_aws\_alb\_target\_group\_name) | n/a |
| <a name="output_ecs_service_arn"></a> [ecs\_service\_arn](#output\_ecs\_service\_arn) | n/a |
| <a name="output_ecs_service_name"></a> [ecs\_service\_name](#output\_ecs\_service\_name) | n/a |

## Usage

```hcl-terraform
module "ecs_service_in_cluster_frontend" {
    source = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/ecs_service_in_cluster"
    aws_ecs_cluster_id =  <"AWS ECS cluster ID>
    vpc_id = "<VPC ID>"

    load_balancer_arn =  "<Application Load Balancer ARN>"
    ecs_iam_role_arn =  "<IAM role ARN for ECS>"
    acm_certificate_arn = "<AWS Amazon certificate ARN>"

    default_target_group_arn = "" # Put empty string if this is the main application service, otherwise provide an ARN of target group associated to other application
    create_aws_alb_listener_rule = false # Provide false if this is the main application service, otherwise provide true which will create a custom listen rule for this application service

    service = {
        name = "<Name of your service>" # only alphanumeric characters and hyphens allowed
        container_name = "<Container name>"
        container_port = "<Container port>"
        host_port      = <Host port>
        protocol       = "<Protocol>"
        health_check_path = "<Path for health check>"
        desired_count = 1 # A number of desired service

        task_definition_arn  = "Task definition ARN"
    }
}
```
